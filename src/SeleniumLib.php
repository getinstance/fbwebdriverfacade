<?php
namespace getinstance\fbwebdriverfacade;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverSelect;

/**
 * Class SeleniumLib
 * @package getinstance\fbwebdriverfacade
 */
class SeleniumLib
{

    /**
     * @var RemoteWebDriver
     */
    private $driver;

    /**
     * SeleniumLib constructor.
     * @param RemoteWebDriver $driver
     */
    function __construct(RemoteWebDriver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param $partial
     * @return bool
     */
    function partialHrefExists($partial)
    {
        $xpath = "//a[contains(@href, '{$partial}')]";
        try {
            $link= $this->driver->findElement(WebDriverBy::xpath($partial));
            return true;
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @param $partial
     */
    function waitForPartialHref($partial)
    {
        $xpath = "//a[contains(@href, '{$partial}')]";
        return $this->waitForXpathVisible($xpath);
    }

    /**
     * @param $partial
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickLinkAtPartialHref($partial)
    {
        $this->waitForPartialHref($partial);

        $xpath = "//a[contains(@href, '{$partial}')]";
        $sub = $this->driver->findElement(WebDriverBy::xpath($xpath));
        $sub->sendkeys("\n");
        return $sub;
    }

    /**
     * @param $partial
     */
    function waitForLink($partial)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::partialLinkText($partial)
            )
        );
    }

    /**
     * @param $partial
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickLinkExact($partial)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::linkText($partial)
            )
        );
        $link= $this->driver->findElement(WebDriverBy::LinkText($partial));
        $this->myclick($link);
        return $link;
    }

    /**
     * @param $partial
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickLink($partial)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::partialLinkText($partial)
            )
        );
        $link= $this->driver->findElement(WebDriverBy::partialLinkText($partial));
        $this->myclick($link);
        return $link;
    }

    /**
     * @param $id
     * @return bool
     */
    function idVisible($id)
    {
        if (! $this->idExists($id)) {
            return false;
        }
        $el= $this->driver->findElement(WebDriverBy::id($id));
        return $el->isDisplayed();
    }

    /**
     * @param $name
     * @return bool
     */
    function nameExists($name)
    {
        try {
            $el = $this->driver->findElement(WebDriverBy::name($name));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    function idExists($id)
    {
        try {
            $el= $this->driver->findElement(WebDriverBy::id($id));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    function classExists($id)
    {
        try {
            $el = $this->driver->findElement(WebDriverBy::className($id));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $partial
     * @return bool
     */
    function linkVisible($partial)
    {
        if (! $this->linkExists($partial)) {
            return false;
        }
        $link = $this->driver->findElement(WebDriverBy::partialLinkText($partial));
        return $link->isDisplayed();
    }

    /**
     * @param $partial
     * @return bool
     */
    function linkExists($partial)
    {
        try {
            $this->waitForLink($partial);
            $link= $this->driver->findElement(WebDriverBy::partialLinkText($partial));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @param $attribute
     * @return null|string
     */
    function attributeForId($id, $attribute)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        $val = $el->getAttribute($attribute);
        return $val;
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtName($name)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));
        $this->myclick($el);
        return $el;
    }

    /**
     * @param $id
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtId($id)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        $this->myclick($el);
        return $el;
    }

    /**
     * @param $selector
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtCss($selector)
    {
        $this->waitForCss($selector);
        $schemel = $this->driver->findElement(WebDriverBy::cssSelector($selector));
        $this->myclick($schemel);
        return $schemel;
    }

    /**
     * @param $classname
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtClass($classname)
    {
        $this->waitForClass($classname);
        $schemel = $this->driver->findElement(WebDriverBy::className($classname));
        $this->myclick($schemel);
        return $schemel;
    }

    /**
     * @param $key
     * @param $val
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtAttributeWithVal($key, $val)
    {
        $xpath = "//*[@{$key}='{$val}']";
        $this->waitForXpath($xpath);
        $sub = $this->driver->findElement(
            WebDriverBy::xpath($xpath)
        );
        $this->myclick($sub);
        return $sub;
    }

    // not currently used - treat as untested

    /**
     * @param $name
     */
    function clickParent($name)
    {
        $sub = $this->driver->findElement(WebDriverBy::xpath(
            "//*[@name='{$name}']/.."
        ));
        $this->myclick($sub);
    }

    /**
     * @param $xpath
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickAtXpath($xpath)
    {
        $this->waitForXpath($xpath);
        $sub = $this->driver->findElement(WebDriverBy::xpath($xpath));
        $this->myclick($sub);
        return $sub;
    }

    /**
     * @param $txt
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickElContainingText($txt)
    {
        $sub = $this->driver->findElement(WebDriverBy::xpath(
            "//*[contains(text(),'{$txt}')]"
        ));
        $this->myclick($sub);
        return $sub;
    }

    /**
     * @param $val
     * @throws \Exception
     */
    function clickButton($val)
    {
        $myexception = null;

        $css_vals = array(
            "input[type='submit'][value='{$val}']",
            "button[value='{$val}']"
        );
        foreach ($css_vals as $css) {
            try {
                $sub = $this->driver->findElement(WebDriverBy::cssSelector($css));
                if (is_object($sub)) {
                    $this->myclick($sub);
                    return;
                }
            } catch (\Exception $e) {
                $myexception = $e;
            }
        }
        throw $myexception;
    }

    /**
     * @param RemoteWebElement $el
     */
    function myclick($el)
    {
        $el->sendKeys("\n");
    }

    /**
     * @param $title
     * @return bool
     */
    function switchToWin($title)
    {
        $found = false;
        $handles = $this->driver->getWindowHandles();
        foreach ($handles as $handle) {
            $this->driver->switchTo()->window($handle);
            if ($this->driver->getTitle() == $title) {
                $found = true;
                continue;
            }
        }
        return $found;
    }

    /**
     * @param $id
     * @param $val
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function typeInAtClass($id, $val)
    {
        $this->waitForClass($id);
        $schemel = $this->driver->findElement(WebDriverBy::classname($id));
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    /**
     * @param $id
     * @param $val
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function typeInAtId($id, $val)
    {
        $this->waitForId($id);
        $schemel = $this->driver->findElement(WebDriverBy::id($id));
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    /**
     * @param $name
     * @param $val
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function typeIn($name, $val)
    {
        $this->waitForName($name);
        $schemel = $this->driver->findElement(WebDriverBy::name($name));
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    /**
     * @param $classname
     * @return mixed
     */
    function clickAtClassFirstVisible($classname)
    {
        $els = $this->driver->findElements(WebDriverBy::className($classname));
        $callback = function ($subel) {
            $this->myclick($subel);
        };
        return $this->doFirstVisible($classname, $els, $callback);
    }

    /**
     * @param $name
     * @return mixed
     */
    function clickCheckFirstVisible($name)
    {
        $els = $this->driver->findElements(WebDriverBy::name($name));
        $callback = function ($subel) {
            $this->myclick($subel);
        };
        return $this->doFirstVisible($name, $els, $callback);
    }

    /**
     * @param $name
     * @param $val
     * @return mixed
     */
    function typeInFirstVisible($name, $val)
    {
        $els = $this->driver->findElements(WebDriverBy::name($name));
        /**
         * @param RemoteWebElement $subel
         */
        $callback = function ($subel) use ($val) {
            $subel->clear();
            $subel->sendKeys($val);
        };
        return $this->doFirstVisible($name, $els, $callback);
    }

    /**
     * @param $name
     * @param array $els
     * @param callable $callback
     * @return mixed
     * @throws \Exception
     */
    function doFirstVisible($name, array $els, callable $callback)
    {
        foreach ($els as $subel) {
            if ($subel->isDisplayed()) {
                $callback( $subel );
                return $subel;
            }
        }
        throw new \Exception("no visible element identified by: $name");
    }

    /**
     * @param $xpath
     * @param $val
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function typeInAtXpath($xpath, $val)
    {
        $schemel = $this->driver->findElement(WebDriverBy::xpath($xpath));
        $schemel->clear();
        $schemel->sendKeys($val);
        return $schemel;
    }

    /**
     * @param $name
     * @param $val
     * @return mixed
     */
    function pickFromSelect($name, $val)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));
        return $this->pickFromSelectAtElement($el, $val);
    }

    /**
     * @param $xpath
     * @param $val
     * @return mixed
     */
    function pickFromSelectAtXpath($xpath, $val)
    {
        $el = $this->driver->findElement(WebDriverBy::xpath($xpath));
        return $this->pickFromSelectAtElement($el, $val);
    }

    /**
     * @param $id
     * @param $val
     * @return mixed
     */
    function pickFromSelectAtId($id, $val)
    {
        $el = $this->driver->findElement(WebDriverBy::xpath("//select[@id='{$id}']"));
        return $this->pickFromSelectAtElement($el, $val);
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getElementByName($name)
    {
        $this->waitForName($name);
        $el = $this->driver->findElement(WebDriverBy::name($name));
        return $el;
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getElementByClass($name)
    {
        $this->waitForClass($name);
        $el = $this->driver->findElement(WebDriverBy::classname($name));
        return $el;
    }

    /**
     * @param $css
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getElementByCSS($css)
    {
        $this->waitForCss($css);
        $els = $this->driver->findElement(WebDriverBy::cssSelector($css));
        return $els;
    }

    /**
     * @param $css
     * @return RemoteWebElement[]
     */
    function getElementsByCss($css)
    {
        $this->waitForCss($css);
        $el = $this->driver->findElements(WebDriverBy::cssSelector($css));
        return $el;
    }

    /**
     * @param $id
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getElementById($id)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        return $el;
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement[]
     */
    function getElementsByClass($name)
    {
        $this->waitForClass($name);
        $el = $this->driver->findElements(WebDriverBy::classname($name));
        return $el;
    }

    /**
     * @param $name
     * @return string
     */
    function getElementValueByName($name)
    {
        $el = $this->getElementByName($name);
        return $el->getText();
    }

    /**
     * @param $name
     * @return null|string
     */
    function getElementValueAttributeByName($name)
    {
        $this->waitForName($name);
        $el = $this->driver->findElement(WebDriverBy::name($name));
        return $el->getAttribute("value");
    }

    /**
     * @param $class
     * @param $tag
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getInnerElementValueByClass($class, $tag)
    {
        $xpath = "//div[contains(@class, '".$class."')]/".$tag;
        $el = $this->driver->findElement(WebDriverBy::xpath($xpath));
        return $el;
    }

    /**
     * @param $id
     * @param $tag
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getInnerElementValueById($id, $tag)
    {
        $xpath = "//div[contains(@id, '".$id."')]/".$tag;
        $el = $this->driver->findElement(WebDriverBy::xpath($xpath));
        return $el;
    }

    /**
     * @param $id
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function findId($id)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        return $el;
    }

    /**
     * @param $id
     * @return null|string
     */
    function getElementValueAttribute($id)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        return $el->getAttribute("value");
    }

    /**
     * @param $id
     * @return string
     */
    function getElementValueByClass($id)
    {
        $this->waitForClass($id);
        $el = $this->driver->findElement(WebDriverBy::className($id));
        return $el->getText();
    }

    /**
     * @param $id
     * @return string
     */
    function getElementValue($id)
    {
        $this->waitForId($id);
        $el = $this->driver->findElement(WebDriverBy::id($id));
        return $el->getText();
    }

    /**
     * @param $name
     * @return string
     */
    function getSelectedValueAttribute($name)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));
        $select = new WebDriverSelect($el);
        $option = $select->getFirstSelectedOption();
        return $option->getAttribute("value");
    }

    /**
     * @param $name
     * @return string
     */
    function getSelectedValueById($name)
    {
        $this->waitForId($name);
        $el = $this->driver->findElement(WebDriverBy::id($name));
        $select = new WebDriverSelect($el);
        $option = $select->getFirstSelectedOption();
        return $option->getText();
    }

    /**
     * @param $name
     * @return string
     */
    function getSelectedValue($name)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));
        $select = new WebDriverSelect($el);
        $option = $select->getFirstSelectedOption();
        return $option->getText();
    }

    /**
     * @param $name
     * @param $idx
     */
    function pickIndexFromSelect($name, $idx)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));
        $select = new WebDriverSelect($el);
        $select->selectByIndex($idx);
    }

    /**
     * @param $el
     * @param $val
     * @return mixed
     */
    function pickFromSelectAtElement($el, $val)
    {
        $select = new WebDriverSelect($el);
        $select->selectByValue($val);
        return $el;
    }

    /**
     * @param $id
     */
    function clickHiddenCheckById($id)
    {
        print "trying to get id - '$id'\n";
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").checked = true;", array());
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function clickCheck($name)
    {
        $check = $this->driver->findElement(WebDriverBy::name($name));
        $this->myclick($check);
        return $check;
    }

    /**
     * @param $xpath
     */
    function clickCheckAtXpath($xpath)
    {
        $check = $this->driver->findElement(WebDriverBy::xpath($xpath));
        $this->myclick($check);
    }

    /**
     * @param $name
     * @param $value
     */
    function clickRadio($name, $value)
    {
        $check = $this->driver->findElement(WebDriverBy::xpath("//input[@name='{$name}'][@value='{$value}']"));
        $this->myclick($check);
    }

    /**
     * @param $name
     */
    function clickHouse($name)
    {
        $check = $this->driver->findElement(WebDriverBy::xpath("//input[@name='{$name}']/following-sibling::span"));
        $this->myclick($check);
    }

    /**
     * @param $name
     */
    function clickHouseAtId($name)
    {
        $check = $this->driver->findElement(WebDriverBy::xpath("//input[@id='{$name}']/following-sibling::span"));
        $this->myclick($check);
    }

    /**
     * @param $name
     */
    function waitForName($name)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::name($name)
            )
        );
    }

    /**
     * @param $name
     */
    function waitForNameVisible($name)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::name($name)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForClassVisible($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::classname($id)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForIdVisible($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::id($id)
            )
        );
    }

    /**
     * @param $css
     */
    function waitForCssVisible($css)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::cssSelector($css)
            )
        );
    }

    /**
     * @param $xpath
     */
    function waitForXpathVisible($xpath)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::xpath($xpath)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForId($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::id($id)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForClass($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::className($id)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForClassVisibility($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::className($id)
            )
        );
    }

    /**
     * @param $id
     */
    function waitForIdVisibility($id)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::id($id)
            )
        );
    }

    /**
     * @param $xpath
     */
    function waitForXpath($xpath)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::xpath($xpath)
            )
        );
    }

    /**
     * @param $xpath
     */
    function waitForCss($xpath)
    {
        $this->driver->wait(20, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(
                WebDriverBy::cssSelector($xpath)
            )
        );
    }

    /**
     * @param $name
     * @param $idx
     */
    function waitForSelectToFillThenPickIndex($name, $idx)
    {
        $el = $this->driver->findElement(WebDriverBy::name($name));

        // this is a dynamically populated pulldown
        // don't select an element until there are some
        // elements to choose from
        $select = new WebDriverSelect($el);
        $this->driver->wait(20, 1000)->until(
            function ($driver) use ($select) {
                $opts = $select->getOptions();
                if (count($opts) > 2) {
                    return true;
                }
                return false;
            },
            "failed to get element count"
        );
        $this->pickIndexFromSelect($name, $idx);
    }

    /**
     * @param $name
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    function getEdit($name)
    {
        sleep(2);
        $css = "textarea[name='{$name}'] + div div[class='note-editable']";
        $this->waitForCssVisible($css);
        $el = $this->driver->findElement(WebDriverBy::cssSelector($css));
        return $el;
    }

    /**
     * @param $name
     * @return string
     */
    function getEditText($name)
    {
        $el = $this->getEdit($name);
        return $el->getText();
    }

    /**
     * @param $name
     * @param $val
     */
    function typeEdit($name, $val)
    {
        $el = $this->getEdit($name);
        $el->clear();
        $el->sendKeys($val);
    }

    /**
     * @param $id
     */
    function clickRadarCheck($id)
    {
        $selector = "input[id='{$id}']";
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").click();", array());
    }

    /**
     * @param $id
     * @return bool
     */
    function radarCheckIsSelected($id)
    {
        $xpath = "//input[@id='{$id}']";
        $el = $this->driver->findElement(WebDriverBy::xpath($xpath));
        $checked = $el->getAttribute("CHECKED");
        return ($checked == "true")?true:false;
    }

    /**
     * @param $id
     */
    function jsClickRadio($id)
    {
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").click();", array());
    }

    /**
     * @param $name
     */
    function jsClickRadioByName($name)
    {
        $element = $this->driver->findElements(WebDriverBy::name($name));
        if ($element) {
            $element[0]->click();
        }
    }

    /**
     * @param $id
     * @param $val
     */
    function jsSetValueAtId($id, $val)
    {
        $result = $this->driver->executeScript("document.getElementById(\"{$id}\").value=\"{$val}\";", array());
    }
}
