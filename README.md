# fbwebdriverfacade

An internal tool we use to shortcut various php-webdriver operations - including workarounds 
for some operations that don't seem to work for us any longer.

Version 1.0.8


```
use getinstance\fbwebdriverfacade\SeleniumLib;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\Remote\RemoteWebDriver;

// you'll need the selenium server running

$host = "http://127.0.0.1:7772/wd/hub";
$coreurl = "http://www.google.com";
$capabilities = array(WebDriverCapabilityType::BROWSER_NAME => 'firefox');
$driver = RemoteWebDriver::create($host, $capabilities);
$tools = new SeleniumLib( $driver );
$driver->get($coreurl);

$tools->typeIn("q", "fbwebdriverfacade");
$tools->clickButton("Google Search");
$tools->waitForId("resultStats");
$tools->clickLink("Videos");

```
